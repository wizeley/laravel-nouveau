<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UuidModel extends Model
{
    //https://dev.to/bahdcoder_47/eloquent-uuids-eni
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */

    //Set the incrementing property on the model to false:
    public $incrementing = false;

    /**
     * Registering eloquent creating hook
     * On the model, we will register a creating hook,
     * which laravel will execute before making a database query to save the record.
     * Boot the Model.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($instance) {
            $instance->id = uuid4();
        });
    }


    /**
     * That's it ! Anytime we create a new database record of the User model,
     * a UUID is generated and automatically saved into the database for it.
     */


}

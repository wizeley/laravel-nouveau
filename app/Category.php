<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends UuidModel
{
    //
    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->slug = str_slug($model->name);
        });
    }

    protected $fillable = ['id','name','slug','parentCategory','isVisible'];
}

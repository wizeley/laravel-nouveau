<?php

use App\Category;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\CategoryCollection;
//Use App\Http\Controllers\Api\v1

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::resource('products', 'ProductController');
//Route::resource('categories', 'CategoryController');

Route::get('/', function () {
    return \App\Category::create([
        'name' => 'Toys R Us',
        'slug' => 'toys',
        'parentCategory' => '2',
        'isVisible' => '1',
    ]);
});

Route::get('/json/categories', function () {
    $categories = Category::all();
    return new CategoryCollection($categories);
});

//TODO 2
Route::get('/json/{catid}', function ($catid)
{
    $category = Category::find(1);
    return new CategoryResource($category);
});


Route::get('/test', function () {
    $title = str_slug("Laravel 5 Framework", "-");
    return $title;
});


//Route::resource('api/categories', 'CategoryApiController');

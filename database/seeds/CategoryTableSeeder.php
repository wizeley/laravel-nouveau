<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('categories')->truncate();
        \DB::table('categories')->insert(['name' => 'Beauty']);
        \DB::table('categories')->insert(['slug' => 'beauty']);
        \DB::table('categories')->insert(['name' => 'Hair']);
        \DB::table('categories')->insert(['name' => 'Skin']);
        \DB::table('categories')->insert(['name' => 'Makeup']);
    }
}
